package middleware

import (
	"time"
	"widi_request_sim_manager/requests"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofrs/uuid"
	"github.com/sirupsen/logrus"
)

type Authentication struct {
	UUID   string `json:"uuid"`
	UserID string `json:"user_id"`
	jwt.StandardClaims
}

func GenerateTokenJWT(user *requests.GetUser) (*string, error) {
	newUUID, _ := uuid.NewV4()
	sign := jwt.NewWithClaims(
		jwt.SigningMethodHS256,
		Authentication{
			StandardClaims: jwt.StandardClaims{
				Issuer:    "auth",
				ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
			},
			UserID: user.Uuid.String(),
			UUID:   newUUID.String(),
		},
	)
	token, err := sign.SignedString([]byte(`secret`))
	if err != nil {
		logrus.Error(err.Error())
		return nil, err
	}

	return &token, nil
}
