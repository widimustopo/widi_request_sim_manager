package main

import (
	"widi_request_sim_manager/database"
	config "widi_request_sim_manager/libs"
	"widi_request_sim_manager/responses"
	domain "widi_request_sim_manager/routes"
	"widi_request_sim_manager/validator"

	"widi_request_sim_manager/clients/rabbitmq"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

func main() {

	//load config from config.yml
	loadConfig := config.LoadConfig()

	//load config rabbitmq
	rabbitmq.Init(&loadConfig.Amqp)

	//initiate database connection
	db := database.OpenDB(loadConfig)

	router := echo.New()

	//initiate validator
	router.Validator = validator.NewValidator()

	//custom response for not matching routes
	echo.NotFoundHandler = func(c echo.Context) error {
		// render 404 custom response
		return responses.NotFound(c, "Not Matching of Any Routes", nil, "Not Found")
	}

	domain.Routes(router, db)

	err := router.Start(loadConfig.ServerPort)
	if err != nil {
		logrus.Info(err)
		panic(err)
	}

}
