package rabbitmq

import (
	"fmt"
	"time"

	"widi_request_sim_manager/libs"
	"widi_request_sim_manager/requests"

	"encoding/json"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type RabbitMQ struct {
	URL                            string
	RabbitMQRequestQueue           string
	RabbitMQExchangeRequestDeclare string
	RabbitMQExchangeRequest        string
	RabbitMqRequestBind            string
	//edit
	RabbitMQExchangeEditRequest        string
	RabbitMQExchangeEditRequestDeclare string
	RabbitMQEditRequestQueue           string
	RabbitMqEditRequestBind            string

	//delete
	RabbitMQExchangeDeleteRequest        string
	RabbitMQExchangeDeleteRequestDeclare string
	RabbitMQDeleteRequestQueue           string
	RabbitMqDeleteRequestBind            string
	//Connections
	Conn       *amqp.Connection
	Chann      *amqp.Channel
	closeChann chan *amqp.Error
	quitChann  chan bool
}

var RabbitMQClient RabbitMQ

func Init(config *libs.AMQP) *RabbitMQ {

	rmq := &RabbitMQ{
		URL: config.RabbitMQURL,
		//add
		RabbitMQRequestQueue:           config.RabbitMQRequestQueue,
		RabbitMQExchangeRequestDeclare: config.RabbitMQExchangeRequestDeclare,
		RabbitMQExchangeRequest:        config.RabbitMQExchangeRequest,
		RabbitMqRequestBind:            config.RabbitMqRequestBind,
		//edit
		RabbitMQExchangeEditRequest:        config.RabbitMQExchangeEditRequest,
		RabbitMQExchangeEditRequestDeclare: config.RabbitMQExchangeEditRequestDeclare,
		RabbitMQEditRequestQueue:           config.RabbitMQEditRequestQueue,
		RabbitMqEditRequestBind:            config.RabbitMqEditRequestBind,
		//delete
		RabbitMQExchangeDeleteRequest:        config.RabbitMQExchangeDeleteRequest,
		RabbitMQExchangeDeleteRequestDeclare: config.RabbitMQExchangeDeleteRequestDeclare,
		RabbitMQDeleteRequestQueue:           config.RabbitMQDeleteRequestQueue,
		RabbitMqDeleteRequestBind:            config.RabbitMqDeleteRequestBind,
	}

	err := rmq.startConnection()
	if err != nil {
		log.Fatalf("Connection RabbitMQ: %v", err)
	}

	rmq.quitChann = make(chan bool)

	RabbitMQClient = *rmq

	go rmq.handleDisconnect()
	return rmq
}

func (rmq *RabbitMQ) startConnection() error {

	var err error
	rmq.Conn, err = amqp.Dial(rmq.URL)
	if err != nil {
		return err
	}
	rmq.Chann, err = rmq.Conn.Channel()
	if err != nil {
		return err
	}

	args := make(amqp.Table)
	//create channel exchange for add
	err = rmq.Chann.ExchangeDeclare(rmq.RabbitMQExchangeRequestDeclare, "direct", true, false, false, false, nil)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	args["x-delayed-type"] = "direct"
	err = rmq.Chann.ExchangeDeclare(rmq.RabbitMQExchangeRequest, "x-delayed-message", true, false, false, false, args)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	//create channel exchange for edit
	err = rmq.Chann.ExchangeDeclare(rmq.RabbitMQExchangeEditRequestDeclare, "direct", true, false, false, false, nil)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	args["x-delayed-type"] = "direct"
	err = rmq.Chann.ExchangeDeclare(rmq.RabbitMQExchangeEditRequest, "x-delayed-message", true, false, false, false, args)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	//create channel exchange for delete
	err = rmq.Chann.ExchangeDeclare(rmq.RabbitMQExchangeDeleteRequestDeclare, "direct", true, false, false, false, nil)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	args["x-delayed-type"] = "direct"
	err = rmq.Chann.ExchangeDeclare(rmq.RabbitMQExchangeDeleteRequest, "x-delayed-message", true, false, false, false, args)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	return nil
}

func PublishRequest(req requests.AddRequest) {
	rmq := RabbitMQClient
	body, _ := json.Marshal(req)

	err := rmq.Chann.Publish(rmq.RabbitMQExchangeRequest, rmq.RabbitMqRequestBind, false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		Timestamp:    time.Now(),
		ContentType:  "application/json",
		Body:         body,
	})
	if err != nil {
		log.Error(err.Error())
	}

	log.Info("Succes publish data request : ", req)
}

func PublishEditRequest(req requests.EditRequest) {
	rmq := RabbitMQClient
	body, _ := json.Marshal(req)

	err := rmq.Chann.Publish(rmq.RabbitMQExchangeEditRequest, rmq.RabbitMqEditRequestBind, false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		Timestamp:    time.Now(),
		ContentType:  "application/json",
		Body:         body,
	})
	if err != nil {
		log.Error(err.Error())
	}

	log.Info("Succes publish data edit request : ", req)
}

func PublishDeleteRequest(req requests.DeleteRequest) {
	rmq := RabbitMQClient
	body, _ := json.Marshal(req)

	err := rmq.Chann.Publish(rmq.RabbitMQExchangeDeleteRequest, rmq.RabbitMqDeleteRequestBind, false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		Timestamp:    time.Now(),
		ContentType:  "application/json",
		Body:         body,
	})
	if err != nil {
		log.Error(err.Error())
	}

	log.Info("Succes publish data delete request : ", req)
}

// handle a disconnection trying to reconnect every 5 seconds
func (rmq *RabbitMQ) handleDisconnect() {
	for {
		select {
		case errChann := <-rmq.closeChann:
			if errChann != nil {
				log.Errorf("rabbitMQ disconnection: %v", errChann)
			}
		case <-rmq.quitChann:
			rmq.Conn.Close()
			log.Info("rabbitMQ has been shut down")
			rmq.quitChann <- true
			return
		}
		log.Info("trying to reconnect to rabbitMQ...")
		time.Sleep(5 * time.Second)
		if err := rmq.startConnection(); err != nil {
			log.Errorf("rabbitMQ error: %v", err)
		}
	}
}
