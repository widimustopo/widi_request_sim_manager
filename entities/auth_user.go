package entities

import "github.com/gofrs/uuid"

type AuthUsers struct {
	Uuid  uuid.UUID `json:"uuid"`
	Token string    `json:"token"`
}

type Users struct {
	Uuid     uuid.UUID `json:"uuid"`
	Username string    `json:"username"`
	Password string    `json:"password"`
}
