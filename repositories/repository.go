package repositories

import (
	"widi_request_sim_manager/entities"
	"widi_request_sim_manager/requests"
)

type Repositories interface {
	AddRequest(req requests.AddRequest) (*entities.Requester, error)
	EditRequest(uuid string, req requests.EditRequest) (*entities.Requester, error)
	DeleteRequest(uuid string, req requests.DeleteRequest) error
	GetRequest(uuid string, req requests.GetRequest) (*entities.Requester, error)
}

type Users interface {
	Login(req requests.GetUser) (*entities.AuthUsers, error)
}
