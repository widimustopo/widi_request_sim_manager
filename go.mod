module widi_request_sim_manager

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/labstack/echo/v4 v4.3.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.8.1
	github.com/streadway/amqp v1.0.0
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.9
)
