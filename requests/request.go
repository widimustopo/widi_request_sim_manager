package requests

import (
	"github.com/gofrs/uuid"
)

type AddRequest struct {
	IdRequest     uuid.UUID `json:"id_request" gorm:"primary_key; unique;type:uuid; column:id_request;default:uuid_generate_v4()"`
	NoKtp         string    `json:"no_ktp" validate:"required"`
	FullName      string    `json:"full_name" validate:"required"`
	Address       string    `json:"address" validate:"required"`
	Gender        string    `json:"gender" validate:"required"`
	Religion      string    `json:"religion" validate:"required"`
	Work          string    `json:"work" validate:"required"`
	Status        string    `json:"status" validate:"required"`
	StatusRequest int8      `json:"status_request"`
}

type EditRequest struct {
	IdRequest     uuid.UUID `json:"id_request"`
	NoKtp         string    `json:"no_ktp"`
	FullName      string    `json:"full_name"`
	Address       string    `json:"address" `
	Gender        string    `json:"gender" `
	Religion      string    `json:"religion" `
	Work          string    `json:"work" `
	Status        string    `json:"status"`
	StatusRequest int8      `json:"status_request"`
}

type DeleteRequest struct {
	IdRequest uuid.UUID `json:"id_request"`
}

type GetRequest struct {
	IdRequest uuid.UUID `json:"id_request"`
}

type GetUser struct {
	Uuid     uuid.UUID `json:"uuid"`
	Username string    `json:"username"  validate:"required"`
	Password string    `json:"password"  validate:"required"`
}
