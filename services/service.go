package services

import (
	"errors"
	"widi_request_sim_manager/clients/rabbitmq"
	"widi_request_sim_manager/entities"
	"widi_request_sim_manager/middleware"
	"widi_request_sim_manager/repositories"
	"widi_request_sim_manager/requests"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type Service struct {
	*gorm.DB
}

func NewService(db *gorm.DB) repositories.Repositories {
	return Service{db}
}

func NewUserService(db *gorm.DB) repositories.Users {
	return Service{db}
}

func (s Service) AddRequest(req requests.AddRequest) (*entities.Requester, error) {
	req.StatusRequest = 0
	go rabbitmq.PublishRequest(req)

	privatekey, resBytes, err := middleware.CreateRsaSignatureOnAdd(req)
	if err != nil {
		logrus.Error(err.Error())
		return nil, err
	}

	resDec, err := middleware.DecryptRsa(privatekey, resBytes)
	if err != nil {
		logrus.Error(err.Error())
		return nil, err
	}

	if req.NoKtp != resDec {
		return nil, errors.New("Woops Its not your KTP")
	}

	result, err := s.GetRequesterDetails(req.IdRequest.String(), req.NoKtp)
	if err != nil {
		logrus.Error(err.Error())
		return nil, err
	}

	return &entities.Requester{
		IdRequest:     result.IdRequest,
		NoKtp:         result.NoKtp,
		FullName:      result.FullName,
		Address:       result.Address,
		Gender:        result.Gender,
		Religion:      result.Religion,
		Work:          result.Work,
		Status:        result.Status,
		StatusRequest: result.StatusRequest,
	}, nil
}
func (s Service) EditRequest(uuid string, req requests.EditRequest) (*entities.Requester, error) {
	_, err := s.FindUserByUuid(uuid)
	if err != nil {
		logrus.Error(err.Error())
		return nil, err
	}
	go rabbitmq.PublishEditRequest(req)

	result, err := s.GetRequesterDetails(req.IdRequest.String(), "")
	if err != nil {
		logrus.Error(err.Error())
		return nil, err
	}

	return &entities.Requester{
		IdRequest:     result.IdRequest,
		NoKtp:         result.NoKtp,
		FullName:      result.FullName,
		Address:       result.Address,
		Gender:        result.Gender,
		Religion:      result.Religion,
		Work:          result.Work,
		Status:        result.Status,
		StatusRequest: result.StatusRequest,
	}, nil
}

func (s Service) DeleteRequest(uuid string, req requests.DeleteRequest) error {
	_, err := s.FindUserByUuid(uuid)
	if err != nil {
		logrus.Error(err.Error())
		return err
	}
	go rabbitmq.PublishDeleteRequest(req)
	return nil
}

func (s Service) GetRequest(uuid string, req requests.GetRequest) (*entities.Requester, error) {
	_, err := s.FindUserByUuid(uuid)
	if err != nil {
		logrus.Error(err.Error())
		return nil, err
	}

	result, err := s.GetRequesterDetails(req.IdRequest.String(), "")
	if err != nil {
		logrus.Error(err.Error())
		return nil, err
	}

	return &entities.Requester{
		IdRequest:     result.IdRequest,
		NoKtp:         result.NoKtp,
		FullName:      result.FullName,
		Address:       result.Address,
		Gender:        result.Gender,
		Religion:      result.Religion,
		Work:          result.Work,
		Status:        result.Status,
		StatusRequest: result.StatusRequest,
	}, nil
}

func (s Service) Login(req requests.GetUser) (*entities.AuthUsers, error) {
	result, err := s.FindByCridential(req.Username, req.Password)
	if err != nil || result == nil {
		logrus.Error(err.Error())
		return nil, err
	}
	req.Uuid = result.Uuid
	token, err := middleware.GenerateTokenJWT(&req)
	if err != nil {
		logrus.Error(err.Error())
		return nil, err
	}

	return &entities.AuthUsers{
		Uuid:  req.Uuid,
		Token: *token,
	}, nil
}

func (s Service) FindByCridential(username string, password string) (*entities.Users, error) {
	var user entities.Users

	err := s.DB.First(&user, "username = ? AND password = ?", username, password).Error
	if err != nil {
		logrus.Info("err ", user)
		return nil, err
	}
	return &user, nil
}

func (s Service) FindUserByUuid(Uuid string) (*entities.Users, error) {
	var user entities.Users

	err := s.DB.First(&user, "uuid = ?", Uuid).Error
	if err != nil {
		logrus.Info("err ", user)
		return nil, err
	}
	return &user, nil
}

func (s Service) GetRequesterDetails(idRequest, NoKtp string) (*entities.Requester, error) {
	var requester entities.Requester

	err := s.DB.First(&requester, "id_request = ? OR no_ktp = ?", idRequest, NoKtp).Error
	if err != nil {
		logrus.Info("err ", requester)
		return nil, err
	}
	return &requester, nil
}
