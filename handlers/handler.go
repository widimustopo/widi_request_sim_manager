package handlers

import (
	"context"

	"widi_request_sim_manager/middleware"
	"widi_request_sim_manager/repositories"
	"widi_request_sim_manager/requests"
	"widi_request_sim_manager/responses"
	"widi_request_sim_manager/services"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"

	"widi_request_sim_manager/libs"

	"github.com/gofrs/uuid"
)

type ServiceHandler struct {
	context    context.Context
	repository repositories.Repositories
	user       repositories.Users
	db         *gorm.DB
}

func NewServiceHandler(db *gorm.DB) *ServiceHandler {

	repo := services.NewService(db)
	user := services.NewUserService(db)

	var ctx context.Context

	return &ServiceHandler{
		context:    ctx,
		repository: repo,
		user:       user,
		db:         db,
	}
}

func (s ServiceHandler) Login(ctx echo.Context) error {
	var req requests.GetUser
	if err := ctx.Bind(&req); err != nil {
		return responses.BadRequest(ctx, libs.BadRequest, nil, err.Error())
	}

	if err := ctx.Validate(req); err != nil {
		return responses.ValidationError(ctx, libs.ValidationError, nil, err.Error())
	}

	data, err := s.user.Login(req)
	if err != nil {
		return responses.InternalServerError(ctx, libs.InternalServerError, nil, err.Error())
	}

	return responses.SingleData(ctx, libs.OK, data, nil)
}

func (s ServiceHandler) AddRequest(ctx echo.Context) error {
	var req requests.AddRequest

	if err := ctx.Bind(&req); err != nil {
		return responses.BadRequest(ctx, libs.BadRequest, nil, err.Error())
	}

	if err := ctx.Validate(req); err != nil {
		return responses.ValidationError(ctx, libs.ValidationError, nil, err.Error())
	}

	data, err := s.repository.AddRequest(req)
	if err != nil {
		return responses.InternalServerError(ctx, libs.InternalServerError, nil, err.Error())
	}

	return responses.SingleData(ctx, libs.OK, data, nil)
}

func (s ServiceHandler) EditRequest(ctx echo.Context) error {
	idRequest := ctx.Param("id_request")

	userAccess := ctx.Get("user").(*jwt.Token)

	getUserFromToken := userAccess.Claims.(*middleware.Authentication)
	Uuid := getUserFromToken.UserID

	var req requests.EditRequest

	if err := ctx.Bind(&req); err != nil {
		return responses.BadRequest(ctx, libs.BadRequest, nil, err.Error())
	}

	if err := ctx.Validate(req); err != nil {
		return responses.ValidationError(ctx, libs.ValidationError, nil, err.Error())
	}

	newUuid, _ := uuid.FromString(idRequest)
	req.IdRequest = newUuid
	data, err := s.repository.EditRequest(Uuid, req)
	if err != nil {
		return responses.InternalServerError(ctx, libs.InternalServerError, nil, err.Error())
	}

	return responses.SingleData(ctx, libs.OK, data, nil)
}

func (s ServiceHandler) DeleteRequest(ctx echo.Context) error {
	idRequest := ctx.Param("id_request")
	userAccess := ctx.Get("user").(*jwt.Token)

	getUserFromToken := userAccess.Claims.(*middleware.Authentication)
	Uuid := getUserFromToken.UserID

	var req requests.DeleteRequest

	if err := ctx.Bind(&req); err != nil {
		return responses.BadRequest(ctx, libs.BadRequest, nil, err.Error())
	}

	if err := ctx.Validate(req); err != nil {
		return responses.ValidationError(ctx, libs.ValidationError, nil, err.Error())
	}

	newUuid, _ := uuid.FromString(idRequest)
	req.IdRequest = newUuid
	err := s.repository.DeleteRequest(Uuid, req)
	if err != nil {
		return responses.InternalServerError(ctx, libs.InternalServerError, nil, err.Error())
	}

	return responses.SingleData(ctx, libs.DeleteSuccess, nil, nil)
}

func (s ServiceHandler) GetRequest(ctx echo.Context) error {
	idRequest := ctx.Param("id_request")
	userAccess := ctx.Get("user").(*jwt.Token)

	getUserFromToken := userAccess.Claims.(*middleware.Authentication)
	Uuid := getUserFromToken.UserID
	var getDetail requests.GetRequest

	newUuid, _ := uuid.FromString(idRequest)
	getDetail.IdRequest = newUuid
	data, err := s.repository.GetRequest(Uuid, getDetail)
	if err != nil {
		return responses.InternalServerError(ctx, libs.InternalServerError, nil, err.Error())
	}

	return responses.SingleData(ctx, libs.Success, data, nil)
}
