package routes

import (
	"widi_request_sim_manager/handlers"
	"widi_request_sim_manager/middleware"

	"github.com/labstack/echo/v4"
	echoMiddleware "github.com/labstack/echo/v4/middleware"
	"gorm.io/gorm"
)

func Routes(router *echo.Echo, db *gorm.DB) {
	serviceHandler := handlers.NewServiceHandler(db)
	router.POST("/requests/sims", serviceHandler.AddRequest)
	router.PATCH("/requests/sims/:id_request", serviceHandler.EditRequest, echoMiddleware.JWTWithConfig(echoMiddleware.JWTConfig{
		Claims:     &middleware.Authentication{},
		SigningKey: []byte("secret"),
	}))
	router.GET("/requests/sims/:id_request", serviceHandler.GetRequest, echoMiddleware.JWTWithConfig(echoMiddleware.JWTConfig{
		Claims:     &middleware.Authentication{},
		SigningKey: []byte("secret"),
	}))
	router.DELETE("/requests/sims/:id_request", serviceHandler.DeleteRequest, echoMiddleware.JWTWithConfig(echoMiddleware.JWTConfig{
		Claims:     &middleware.Authentication{},
		SigningKey: []byte("secret"),
	}))
	router.POST("/login", serviceHandler.Login)
}
